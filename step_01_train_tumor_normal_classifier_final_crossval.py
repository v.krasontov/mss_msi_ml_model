
# set hyperparameters
# get model pretrained on imagenet
# modify model - freeze beginning layers,
#   replace classification layer with our classes
# load images
# preprocess images (at least resize to fit into trained model)
# may also augment images to get more data
# divide images into train + validation (before augmentation!?)
# train model
# validate model
